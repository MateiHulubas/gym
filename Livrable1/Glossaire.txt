Glossaire : 
 1) Yoga : Ensemble d'exercices de relaxation mentale et physiques qui tire ses origines en Inde
2) Cross-fit : Un type d'entrainement qui se distingue par des mouvements fonctionels variés constamment et executés à haute intensité.
3)Nutritionniste : Expert de la santé qui se spécialise en alimentation saine et équilibrée
4) Physiothérapeute : Spécialiste de la santé qui vise à ameliorer la mobilité , la motricité et la santé physique à l'aide d'exercices
  bien choisis.
5)Massothérapeute : Personne qui se spécialise dans le massage des tissus musculaires dans le but d'en ameliorer l'état et la circulation sanguine
6)Séance : Une période consacrée à une activité phisique individuelle ou en groupe 