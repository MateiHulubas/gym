package Gym;

public class Professionnel {
	private final int numeroProfessionnel;
    private TypeDeProfessionnel typeDeProfessionnel;    

    Professionnel(String nom, String numero, String email, String infosPersonnelles){
    	super(String nom, String numero, String email, String infosPersonnelles);
        this.numeroProfessionnel = genererNumeroProfessionnel();
    }

    public int getNumeroProfessionnel() { return this.numeroProfessionnel; }
    public TypeDeProfessionnel getTypeDeProfessionnel() { return this.typeDeProfessionnel;}
    
    
    public int genererNumeroProfessionnel(){
    	return 111111111;
    }

    public void creationService(){

    }

    public void miseAJourService(){

    }

    public void suppressionService(){
        
    }


    enum TypeDeProfessionnel {
        EntraineurProfessionnel,
        Yogi,
        EntraineurCrossFit,
        Nutritioniste,
        Physiotherapeute,
        Massotherapeute
    }

}
