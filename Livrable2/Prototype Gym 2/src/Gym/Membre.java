package Gym;
public class Membre extends Client{
	private final int numeroMembre;
    private Statut statut;

    Membre(String nom, String numero, String email, String infosPersonnelles){
    	super(String nom, String numero, String email, String infosPersonnelles);
        this.numeroMembre = genererNumeroMembre();
    }

    public int getNumeroMembre() { return this.numeroMembre; }
    public int getStatut() { return this.statut; }
    
    
    public int genererNumeroMembre(){
    	return 777777777;
    }

    public void inscriptionSeance(){

    }

    public void confirmationPresence(){

    }
    
    public boolean verifierStatut() {
    	
    	switch(this.statut) {
    		case Statut.Valide:
    			return true;
    			break;
    		case Statut.Expire:
    			return false;
    			break;
    		case Statut.Suspendu:
    			return false;
    			break;
    	}
    }
    
    enum Statut {
        Valide,
        Expire,
        Suspendu
    }
    
}
