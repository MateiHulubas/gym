package Gym;

public interface Interface  {
	
	public void ValiderNumeroMembre(); 
	public void CreationService();
	public void ConsulterSeance();
	public void CreationCompte() ;
	public void AccesCentre() ;
	public void InscriptionSeance(); 
	public void ConfirmationPresence(); 
	public void RepertoireSeances() ;
	public void GenererRapport() ;

}
