package Gym;

public class Service {
	 private DateTime dateCreationService;
	 private DateTime dateService;
	 private int numeroProfessionnel;
	 private int numeroMembre;
	 private final int codeService;
	 private float prix;
	 private String commentaires;

	 Service(DateTime dateCreation, DateTime dateService, int numeroProfessionnel, int numeroMembre, float prix, String comments){
		 this.dateCreationService = dateCreation;
		 this.dateService = dateService;
		 this.numeroProfessionnel = numeroProfessionnel;
		 this.numeroMembre = numeroMembre;
		 this.prix = prix;
		 this.commentaires = comments;
		 
		 this.codeService = 2222222;
	 }
	 
	 public DateTime getDateCreationService() {return this.dateCreationService;}
	 public DateTime getDateService() {return this.dateService;}
	 public int getNumeroProfessionnel() {return this.numeroProfessionnel;}
	 public int getNumeroMembre() {return this.numeroMembre;}
	 public int getCodeService() {return this.codeService;}
	 public float getPrix() {return this.prix;}
	 public String getCommentaires() {return this.commentaires;}
	 
	 public void creerService() {
		 
	 }
}
