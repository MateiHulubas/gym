package Gym;

public class Client {
	   private String nom;
	   private String numero;
	   private String email;
	   private String infosPersonnelles;
	    
	   public Client(String nom, String numero, String email, String infosPersonnelles){
		   this.creationClient(nom, numero, email, infosPersonnelles);
	   }

	   public String getNom() { return this.nom; }
	   public String getNumero() { return this.numero; }
	   public String getEmail() { return this.email; }
	   public String getInfosPersonnelles() { return this.infosPersonnelles; }
	   
	   
	   public void creationClient(String nom, String numero, String email, String infosPersonnelles){
		   this.nom = nom;
		   this.numero = numero;
		   this.email = email;
		   this.infosPersonnelles = infosPersonnelles;
	   }

	   public void miseAJourClient(String nom, String numero, String email, String infosPersonnelles){
		   this.nom = nom;
		   this.numero = numero;
		   this.email = email;
		   this.infosPersonnelles = infosPersonnelles;
		   
	   }

	   public void suppressionClient(){
		   this.nom = "";
		   this.numero = "";
		   this.email = "";
		   this.infosPersonnelles = "";
		   
	   }

	   

}
