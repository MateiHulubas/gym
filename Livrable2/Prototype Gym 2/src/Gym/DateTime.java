package Gym;

public class DateTime {
	private int heure;
	private int minutes;
	private int secondes;
	private int annee;
	private int mois;
	private int jour;
	
	DateTime(String date) throws Exception{
		this.setDate(date);
	}

	public void setDate(String date) throws Exception{
		if (date.length() == 19) {
			this.jour = Integer.parseInt(date.substring(0, 2));
			this.mois = Integer.parseInt(date.substring(3, 5));
			this.annee = Integer.parseInt(date.substring(6, 10));
			this.heure = Integer.parseInt(date.substring(11, 13));
			this.minutes = Integer.parseInt(date.substring(14, 16));
			this.secondes = Integer.parseInt(date.substring(17, 19));
		}
		else {
			throw new Exception("Format de Date invalide");
		}
	}
	public String getDate() {
		String string = "";
		string += this.jour + "-" + this.mois + "-" + this.annee + " ";
		string += this.heure + ":" + this.minutes + ":" + this.secondes;
		
		return string;
	}
	
	public int getHeure() {
		return heure;
	}
	public int getMinutes() {
		return minutes;
	}
	public int getSecondes() {
		return secondes;
	}
	public int getAnnee() {
		return annee;
	}
	public int getMois() {
		return mois;
	}
	public int getJour() {
		return jour;
	}

}
