package GYM;

import java.util.ArrayList;


public class CentreDeDonnees {
	private ArrayList<Professionnel> listProfessionnels;
	private ArrayList<Membre> listMembres;
	private ArrayList<ConfirmationSeance> listConfirmationSeances;
	private ArrayList<InscriptionService> listInscriptionServices;
	private ArrayList<Service> listServices;
	private static int numeroCodeMembers;
	private static int numeroCodeProfessionnels;
	
	
	
	CentreDeDonnees(){
		this.listProfessionnels = new ArrayList<Professionnel>();
		this.listMembres = new ArrayList<Membre>();
		this.listConfirmationSeances = new ArrayList<ConfirmationSeance>();
		this.listInscriptionServices = new ArrayList<InscriptionService>();
		this.listServices = new ArrayList<Service>();
		this.numeroCodeMembers = 100000001;
		this.numeroCodeProfessionnels = 900000001;
		
	}
	

	//Fini
	public String validationNumeroMembre(String codeAVerifier) {
		for (Membre membre : listMembres) {
			if (membre.getNumeroMembre().equals(codeAVerifier)) {
				return membre.getStatut();
			}
		}
		
		return "Invalide";
	}
	
	//Fini
	public String creationService(DateTime dateCreation, Date dateDebutService, Date dateFinService, Time heureService, String recurrenceHebdomadaire, int capaciteMaximale, String numeroProfessionnel, float prix, String comments, TypeService typeService) {
		try {
			String codeService = "";
			codeService += typeService.getCode();
			Professionnel professionnel = null;
			for (Professionnel prof : this.listProfessionnels) {
				if (prof.getNumeroProfessionnel().equals(numeroProfessionnel)) {
					professionnel = prof;
				}
			}
			if (professionnel.getNombreServices()>10) {codeService += "0" + professionnel.getNombreServices();}
			else {codeService += professionnel.getNombreServices();}
			codeService += professionnel.getNumeroProfessionnel().substring(7, 9);
			
			Service service = new Service(dateCreation, dateDebutService, dateFinService, heureService, recurrenceHebdomadaire, capaciteMaximale, numeroProfessionnel, prix, codeService, comments, typeService);
			this.listServices.add(service);
			professionnel.addService(service);
			
			return "Service cree \nCode du service: " + codeService;
		}
		catch (Exception e) {
			return "Erreur dans la creation du service";
		}
		
	}
	
	//Fini
	public String creationMembre(String nom, String email, String infosPersonnelles, Boolean soldePaye) {
		try {
			String codeMembre = "" + this.numeroCodeMembers;
			this.numeroCodeMembers++;

			
			Membre membre = new Membre(nom, codeMembre, email, infosPersonnelles, soldePaye);
			this.listMembres.add(membre);
			
			return "Membre cree \nCode du membre: " + codeMembre;
		}
		catch (Exception e) {
			return "Erreur dans la creation du membre";
		}
	}
	
	//Fini
	public String creationProfessionnel(String nom, String email, String infosPersonnelles) {
		try {
			String codeProfessionnel = "" + this.numeroCodeProfessionnels;
			this.numeroCodeProfessionnels++;

			
			Professionnel professionnel = new Professionnel(nom, codeProfessionnel, email, infosPersonnelles);
			this.listProfessionnels.add(professionnel);
			
			return "Professionnel cree \nCode du professionnel: " + codeProfessionnel;
		}
		catch (Exception e) {
			return "Erreur dans la creation du professionnel";
		}
	}
	
	public String consultationRepertoireServices() {
		return "TODO";
	}
	
	public String genererRapport() {
		return "TODO";
	}
	
	public String procedureComptablePrincipale() {
		return "TODO";
	}
	
	public String trouverService() {
		return "TODO";
	}
	
	
	
}










