package GYM;

public class InscriptionService {
	private DateTime dateActuelle;
	private DateTime dateService;
	private int numeroProfessionnel;
	private int numeroMembre;
	private int codeService;

	InscriptionService(DateTime dateActuelle, DateTime dateService, int numeroProfessionnel, int numeroMembre, int codeService){
		this.dateActuelle = dateActuelle;
		this.dateService = dateService;
		this.numeroProfessionnel = numeroProfessionnel;
		this.numeroMembre = numeroMembre;
		this.codeService = codeService;
	}

}
