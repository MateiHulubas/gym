package GYM;

import java.util.ArrayList;

public class Service {
	 private DateTime dateCreationService;
	 private Date dateDebutService;
	 private Date dateFinService;
	 private Time heureService;
	 private String recurrenceHebdomadaire;
	 private int capaciteMaximale;
	 private String numeroProfessionnel;
	 //private ArrayList<Membre> listMembresInscrits;
	 private final String codeService;
	 private float prix;
	 private String commentaires;
	 private TypeService typeService;

	 
	 Service(DateTime dateCreation, Date dateDebutService, Date dateFinService, Time heureService, String recurrenceHebdomadaire, int capaciteMaximale, String numeroProfessionnel, float prix, String codeDuService, String comments, TypeService typeService){
		 this.dateCreationService = dateCreation;
		 this.dateDebutService = dateDebutService;
		 this.dateFinService = dateFinService;
		 this.heureService = heureService;
		 this.recurrenceHebdomadaire = recurrenceHebdomadaire;
		 this.capaciteMaximale = capaciteMaximale;
		 this.numeroProfessionnel = numeroProfessionnel;
		 this.prix = prix;
		 this.commentaires = comments;
		 this.codeService = codeDuService;
		 this.typeService = typeService;
	 }
	 
	 public DateTime getDateCreationService() {return this.dateCreationService;}
	 public Date getDateDebutService() {return this.dateDebutService;}
	 public Date getDateFinService() {return this.dateFinService;}
	 public Time getHeureService() {return this.heureService;}
	 public String getReccurrenceHebdomadaire() {return this.recurrenceHebdomadaire;}
	 private int getCapaciteMaximale() {return this.capaciteMaximale;}
	 public String getNumeroProfessionnel() {return this.numeroProfessionnel;}
	 public String getCodeService() {return this.codeService;}
	 public float getPrix() {return this.prix;}
	 public String getCommentaires() {return this.commentaires;}
	 
	 
}
