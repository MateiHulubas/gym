package GYM;

public class ConfirmationSeance {
	private DateTime dateActuelle;
	private Date dateService;
	private int numeroprofessionnel;
	private int numeroMembre;
	private int codeService;
	private String commentaire;
	
	ConfirmationSeance(DateTime dateActuelle, Date dateService,  int numeroprofessionnel, int numeroMembre, int codeService, String comments){
		this.dateActuelle = dateActuelle;
		this.dateService = dateService;
		this.codeService = codeService;
		this.numeroMembre = numeroMembre;
		this.numeroprofessionnel = numeroprofessionnel;
		this.commentaire = comments;
	}
	
}
