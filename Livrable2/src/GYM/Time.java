package GYM;

public class Time {
	private int heure;
	private int minutes;
	private int secondes;
	
	Time(String date) throws Exception{
		this.setDate(date);
	}

	public void setDate(String date) throws Exception{
		if (date.length() == 8) {
			this.heure = Integer.parseInt(date.substring(0, 2));
			this.minutes = Integer.parseInt(date.substring(3, 5));
			this.secondes = Integer.parseInt(date.substring(6, 8));
		}
		else if (date.length() == 5) {
			this.heure = Integer.parseInt(date.substring(0, 2));
			this.minutes = Integer.parseInt(date.substring(3, 5));
			this.secondes = 0;
		}
		else {
			throw new Exception("Format de Temps invalide");
		}
		
		if (this.heure < 0 && this.minutes < 0 && this.secondes < 0) {
			throw new Exception("Format de Temps invalide");
		}
		
	}
	
	public String getTime() {
		String string = "";
		if (this.heure < 10) {
			string += "0" + this.heure;
		}
		else { string += this.heure; }
		string += ":";
				
		if (this.minutes < 10) {
			string += "0" + this.minutes;
		}
		else { string += this.minutes; }
		
		if (this.secondes < 10 && this.secondes > 0) {
			string += ":0" + this.secondes;
		}
		else if (this.secondes == 0) { return string;}
		else { string += ":" + this.secondes; }
		
		
		return string;
	}
	
	public int getHeure() {
		return this.heure;
	}
	public int getMinutes() {
		return minutes;
	}
	public int getSecondes() {
		return secondes;
	}

	
}