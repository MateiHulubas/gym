package GYM;

public abstract class Client {
	private String nom;
	private final String numero;
	private String email;
	private String infosPersonnelles;

	public Client(String nom, String numero, String email, String infosPersonnelles){
		this.nom = nom;
		this.numero = numero;
		this.email = email;
		this.infosPersonnelles = infosPersonnelles;
	}

	public String getNom() { return this.nom; }
	public String getEmail() { return this.email; }
	public String getInfosPersonnelles() { return this.infosPersonnelles; }
	public String getNumero() { return this.numero; }
	   
	   
	public void miseAJourClient(String nom, String email, String infosPersonnelles){
		this.nom = nom;
		this.email = email;
		this.infosPersonnelles = infosPersonnelles;
		   
	}

	
	/*
	public void suppressionClient(){
		this.nom = "";
		this.email = "";
		this.infosPersonnelles = "";
		   
	   }
	*/
	   

}
