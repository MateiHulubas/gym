package GYM;

public class DateTime {
	private Date date;
	private Time temps;
	
	DateTime(String date) throws Exception{
		this.setDate(date);
	}

	public void setDate(String date) throws Exception{
		if (date.length() == 19) {
			this.date = new Date(date.substring(0, 10));
			this.temps = new Time(date.substring(11, 19));
		}
		else {
			throw new Exception("Format de Date-Temps invalide");
		}
	}
	

	public String getCompleteDate() {
		String string = "";
		string += this.date.getDate() + " ";
		string += this.temps.getTime();
		
		return string;
	}
	
	public String getDateOnly() {
		return this.date.getDate();
	}
	
	public String getTimeOnly() {
		return this.temps.getTime();
	}

	public int getHeure() {
		return this.temps.getHeure();
	}
	public int getMinutes() {
		return this.temps.getMinutes();
	}
	public int getSecondes() {
		return this.temps.getSecondes();
	}
	public int getAnnee() {
		return this.date.getAnnee();
	}
	public int getMois() {
		return this.date.getMois();
	}
	public int getJour() {
		return this.date.getJour();
	}

	
}
