package GYM;

import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;  

public class Console {

	private static CentreDeDonnees centreDeDonnees = new CentreDeDonnees();
	private static java.util.Scanner scanner = new java.util.Scanner(System.in);
	
	public static void main(String[] args) {
		menuPrincipal();
	}
	
	public static void menuPrincipal() {
		String instruction;
		while (true) {
			System.out.println("Menu principal:\n");
			System.out.println("Entrer dans un sous-menu en ecrivant le numero correspondant.");
			System.out.println("Valider Numero Client (1)\n"
					+ "Creation Service (2)\n"						
					+ "Consulter Inscriptions Seance (3)\n"
					+ "Creation Compte (4)\n"						
					+ "Repertoire des services (5)\n"
					+ "Verification Confirmation Service (6)\n"
					+ "Generer Rapport (7)/n"
					+ "Procedure Comptable (8)\n"
					+ "Simuler Application Mobile (9)\n");			
			
			instruction = scanner.nextLine();

			switch (instruction) {
				case "1":
					validerNumeroClient();
					break;
				case "2" :
					System.out.println("(2)");
					break;
				case "3":
					System.out.println("(3)");
					break;
				case "4" :
					System.out.println("(4)");
					break;
				default:
					System.out.println("Mauvais input: sous-menu inexistant.");
					break;
			}
					
		}
	}
	
	public static void validerNumeroClient() {
		System.out.print("Input numero du client pour valider statut: ");
		String instruction = scanner.nextLine();
		String validation;
		validation = centreDeDonnees.validationNumeroMembre(instruction);
		System.out.println(validation);
		
	}
	
	public static void creationService() {
		try {
			DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");  
			LocalDateTime localDateTime = LocalDateTime.now();  
			DateTime dateCreation = new DateTime(dateFormatter.format(localDateTime)); 
			
			System.out.print("Type de Service? (dd-MM-yyyy): ");
			TypeService typeService = TypeService.valueOf(scanner.nextLine());
			
			System.out.print("Debut Service? (dd-MM-yyyy): ");
			Date dateDebutService = new Date(scanner.nextLine());
			
			System.out.print("Fin Service? (dd-MM-yyyy): ");
			Date dateFinService = new Date(scanner.nextLine());
			
			System.out.print("Heure Service? (HH:mm:ss): ");
			Time heureService = new Time(scanner.nextLine());
			
			System.out.print("Reccurence Hebdomadaire? (Les jours): ");
			String reccurenceHebdomadaire = scanner.nextLine();
			
			System.out.print("Capacite Maximale? (Nombre entier): ");
			int capaciteMaximale = Integer.parseInt(scanner.nextLine());
			
			System.out.print("Numero d'identification professionnel? (code): ");
			int codeProf = Integer.parseInt(scanner.nextLine());
			String codeProfessionnel = codeProf + "";
			
			System.out.print("Numero d'identification professionnel? (code): ");
			int codeMemb = Integer.parseInt(scanner.nextLine());
			String codeMembre = codeMemb + "";
			
			System.out.print("Prix? (Nombre a virgule): ");
			float prix = Float.parseFloat(scanner.nextLine());
			
			System.out.print("Commentaires?: ");
			String comments = scanner.nextLine();
			
			
			
			
			
		}
		catch (Exception e){
			System.out.println("Wrong inputs");
		}
		
	}
	
	public static void consulterInscriptionSeance() {
		
	}
	
	public static void creationCompte() {
		
	}
	
	public static void repertoireDesServices() {
		
	}
	
	public static void verificationConfirmationService() {
		
	}
	
	public static void genererRapport() {
		
	}
	
	public static void procedureComptable() {
		
	}

	
	
	public static void SimulerApp() {
		String instruction;
		while (true) {
			System.out.println("Simulation Application Mobile:");
			System.out.println("Inscription Seance (1)\n"
					+ "Confirmation Presence (2)\n"
					+ "Repertoire des Services (3)\n");
			
			instruction = scanner.nextLine();
			
			switch (instruction) {
			case "1":
				System.out.println("(1)");
				break;
			case "2" :
				System.out.println("(2)");
				break;
			case "3":
				System.out.println("(3)");
				break;
			default:
				System.out.println("Mauvais input: sous-menu inexistant.");
				break;
			}
		}
	}

	
	
}
















