package GYM;

public enum TypeService {
	Zumba(883), Nutritionniste(598), Entraineur(634), Yoga(277), CrossFit(159), Physiotherapeute(331), Massotherapeute(472);
	
	private int code;
	
	TypeService(int code){
		this.code = code;
	}
	
	public int getCode() {
		return this.code;
	}
	
}
