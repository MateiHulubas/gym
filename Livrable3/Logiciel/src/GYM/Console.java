package GYM;

import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;  

public class Console {

	private static CentreDeDonnees centreDeDonnees = new CentreDeDonnees();
	private static java.util.Scanner scanner = new java.util.Scanner(System.in);
	private static Boolean run = true;
	
	public static void main(String[] args) {
		menuPrincipal();
	}
	
	//Fini
	public static void menuPrincipal() {
		String instruction;
		while (run) {
			System.out.println("\nMenu principal:");
			System.out.println("Entrer dans un sous-menu en ecrivant le numero correspondant.");
			System.out.println("Valider Numero Client (1)\n"
					+ "Creation Service (2)\n"					
					+ "Creation Compte (3)\n"						
					+ "Repertoire des services (4)\n"
					+ "Generer tous les Rapports (5)\n"
					+ "Simuler Procedure Comptable (6)\n"
					+ "Simuler Application Mobile (7)\n"
					+ "Fermer le programme (8)\n");			
			
			instruction = scanner.nextLine();

			switch (instruction) {
				case "1":
					validerNumeroClient();
					break;
				case "2" :
					creationService();
					break;
				case "3":
					creationCompte();
					break;
				case "4" :
					repertoireDesServicesGym();
					break;
				case "5" :
					genererRapports();
					break;
				case "6" :
					procedureComptable();
					break;
				case "7" :
					SimulerApp();
					break;
				case "8" :
					run = false;
					break;
				default:
					System.out.println("Mauvais input: sous-menu inexistant.");
					break;
			}
					
		}
	}
	
	//Appelé par Menu Principal
	//Fini
	public static void validerNumeroClient() {
		System.out.print("Input numero du client pour valider statut: ");
		String instruction = scanner.nextLine();
		String validation;
		validation = centreDeDonnees.validationNumeroMembre(instruction);
		System.out.println(validation);
	}
	
	//Appelé par Menu Principal
	//Fini
	public static void creationService() {
		try {
			DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");  
			LocalDateTime localDateTime = LocalDateTime.now();  
			DateTime dateCreation = new DateTime(dateFormatter.format(localDateTime)); 
			
			System.out.println("Type de Service?: ");
			System.out.println("(Zumba, Nutritionniste, Entraineur, Yoga, CrossFit, Physiotherapeute, Massotherapeute)");
			String instruction = scanner.nextLine();
			TypeService typeService = TypeService.valueOf(instruction.substring(0, 1).toUpperCase() + instruction.substring(1).toLowerCase());
			
			System.out.print("Debut Service? (dd-MM-yyyy): ");
			Date dateDebutService = new Date(scanner.nextLine());
			
			System.out.print("Fin Service? (dd-MM-yyyy): ");
			Date dateFinService = new Date(scanner.nextLine());
			
			System.out.print("Heure Service? (HH:mm): ");
			Time heureService = new Time(scanner.nextLine());
			
			System.out.print("Reccurence Hebdomadaire? (Les jours): ");
			String recurrenceHebdomadaire = scanner.nextLine();
			
			System.out.print("Capacite Maximale? (Nombre entier): ");
			int capaciteMaximale = Integer.parseInt(scanner.nextLine());
			
			System.out.print("Numero d'identification professionnel? (code): ");
			int codeProf = Integer.parseInt(scanner.nextLine());
			String codeProfessionnel = codeProf + "";
			
			/*System.out.print("Numero d'identification membre? (code): ");
			int codeMemb = Integer.parseInt(scanner.nextLine());
			String codeMembre = codeMemb + "";*/
			
			System.out.print("Prix? (Nombre a virgule): ");
			float prix = Float.parseFloat(scanner.nextLine());
			
			System.out.print("Commentaires?: ");
			String comments = scanner.nextLine();
			
			
			System.out.println(centreDeDonnees.creationService(dateCreation, dateDebutService, dateFinService, heureService, recurrenceHebdomadaire, capaciteMaximale, codeProfessionnel, prix, comments, typeService));
			
			
		}
		catch (Exception e){
			System.out.println("Wrong inputs");
		}
		
	}

	//Appelé par Repertoire des Services Gym
	//Fini
	public static void consulterInscriptionsSeance() {
		System.out.print("Input numero du professionnel: ");
		String instruction = scanner.nextLine();
		String inscriptions;
		inscriptions = centreDeDonnees.consultationInscriptions(instruction);
		System.out.println(inscriptions);
	}
	
	//Appelé par Menu Principal
	//Fini
	public static void creationCompte() {
		try {
			String instruction;
			Boolean isMembre;
			System.out.println("Membre ou Professionnel?");
			instruction = scanner.nextLine();
			switch (instruction) {
				case "Membre":
					isMembre = true;
					break;
				case "membre":
					isMembre = true;
					break;
				case "Professionnel":
					isMembre = false;
					break;
				case "professionnel":
					isMembre = false;
					break;
				default:
					throw new Exception("Type de client inexistant");
			}
			
			if (isMembre) {
				
				//ask name
				System.out.print("Quel est le nom?:");
				String nom = scanner.nextLine();
				
				//ask email
				System.out.print("Quel est le email?:");
				String email = scanner.nextLine();
				
				//ask infosPersonnelles
				System.out.print("Informations Personnelles?:");
				String infosPersonnelles = scanner.nextLine();
				
				//ask soldePaye
				System.out.print("Solde Paye? (Oui/Non)");
				String paye = scanner.nextLine();
				Boolean soldePaye;
				switch (paye) {
					case "Oui":
						soldePaye = true;
						break;
					case "oui":
						soldePaye = true;
						break;
					case "Non":
						soldePaye = false;
						break;
					case "non":
						soldePaye = false;
						break;
					default:
						throw new Exception();
				}
				
				System.out.print(centreDeDonnees.creationMembre(nom, email, infosPersonnelles,soldePaye) + "\n");
			}
			else {
				//ask name
				System.out.print("Quel est le nom?:");
				String nom = scanner.nextLine();
				
				//ask email
				System.out.print("Quel est le email?:");
				String email = scanner.nextLine();
				
				//ask infosPersonnelles
				System.out.print("Informations Personnelles?:");
				String infosPersonnelles = scanner.nextLine();
				
				System.out.println(centreDeDonnees.creationProfessionnel(nom, email, infosPersonnelles));
			}
			
		}
		catch (Exception e) {
			System.out.println("Erreur dans l'entree des inputs");
		}
		
	}

	
	//Appelé par Repertoire des Services Gym
	//Fini
	public static void verificationConfirmationService() {
		System.out.print("Input code du membre: ");
		String codeMembre = scanner.nextLine();
		System.out.print("Input code du service: ");
		String codeService = scanner.nextLine();

		System.out.println(centreDeDonnees.verificationConfirmationService(codeService, codeMembre));
		
	}
	
	//Appelé par Menu Principal
	//Fini
	public static void genererRapports() {
		try {
			System.out.println(centreDeDonnees.genererRapports());
		}
		catch (Exception e) {
			System.out.println("Erreur dans la creation des rapports");
		}
	}
	
	//Appelé par Repertoire des Services Gym
	//Fini
	public static void genererRapportProfessionnel() {
		try {
			String instruction;
			System.out.println("Quel est le code du professionnel?");
			instruction = scanner.nextLine();
			
			System.out.println(centreDeDonnees.genererRapportProfessionnel(instruction));
			
		}
		catch (Exception e) {
			System.out.println("Erreur dans la creation du rapport du professionnel");
		}
	}
	
	//Appelé par App
	//Fini
	public static void genererRapportMembre() {
		try {
			String instruction;
			System.out.println("Quel est le code du membre?");
			instruction = scanner.nextLine();
			
			System.out.println(centreDeDonnees.genererRapportMembre(instruction));
			
		}
		catch (Exception e) {
			System.out.println("Erreur dans la generation du rapport du membre");
		}
	}
	
	//Appelé par Menu Principal
	//TODO
	public static void procedureComptable() {
		System.out.println(centreDeDonnees.procedureComptablePrincipale());
	}

	
	//Appelé par Repertoire des Services App
	//Fini
	public static void inscriptionSeance() {
		try {
			
			System.out.println("Quel est le code du membre?");
			String numeroMembre = scanner.nextLine();
			
			System.out.println("Quel est le code du service?");
			String codeService = scanner.nextLine();

			System.out.println("Quel est la date du service? (JJ-MM-AAAA)");
			Date dateService = new Date(scanner.nextLine());
			
			System.out.println(centreDeDonnees.inscriptionMembre(dateService, numeroMembre, codeService));
			
		}
		catch (Exception e) {
			System.out.println("Erreur dans l'inscription");
		}
	}
	
	//Appelé par Repertoire des Services App
	//Fini
	public static void confirmationPresence() {
		try {
			System.out.println("Quel est le code du membre?");
			String numeroMembre = scanner.nextLine();

			System.out.println("Quel est le code du service?");
			String codeService = scanner.nextLine();
			
			System.out.println("Commentaires?");
			String comments = scanner.nextLine();
			
			System.out.println(centreDeDonnees.confirmationSeance(numeroMembre, codeService, comments));
		}
		catch (Exception e) {
			System.out.println("Erreur dans la confirmation de l'inscription");
		}
		
	}
	
	
	
	/*
	 Other menus
	 */
	
	//Appelé par Menu Principal
	//Fini
	public static void repertoireDesServicesGym() {
		String instruction;
		System.out.println("\nRepertoire des Services:");
		System.out.println("Entrer dans un sous-menu en ecrivant le numero correspondant.");
		System.out.println("Consulter Inscriptions Seance (1)\n"
				+ "Verification Confirmation Service (2)\n"
				+ "Generer Rapport Professionnel (3)");			
		
		instruction = scanner.nextLine();

		switch (instruction) {
			case "1":
				consulterInscriptionsSeance();
				break;
			case "2" :
				verificationConfirmationService();
				break;
			case "3" :
				genererRapportProfessionnel();
				break;
			default:
				System.out.println("Mauvais input: sous-menu inexistant.");
				break;
		}
	}
	
	//Appelé par  App
	//Fini
	public static void repertoireDesServicesApp() {
		String instruction;
		System.out.println("\nRepertoire des Services:");
		System.out.println("Entrer dans un sous-menu en ecrivant le numero correspondant.");
		System.out.println("Inscription Seance (1)\n"
				+ "Confirmation Presence (2)\n"
				+ "Generer Rapport Membre (3)\n");			
		
		instruction = scanner.nextLine();

		switch (instruction) {
			case "1":
				inscriptionSeance();
				break;
			case "2" :
				confirmationPresence();
				break;
			case "3" :
				genererRapportMembre();
				break;
			default:
				System.out.println("Mauvais input: sous-menu inexistant.");
				break;
		}
	}
	
	//Fini
	public static void SimulerApp() {
		String instruction;
		Boolean appRun = true;
		while (appRun) {
			System.out.println("\nSimulation Application Mobile:\n");
			System.out.println("Repertoire des Services (1)\n"
					+ "Menu Principal (2)\n");
			
			instruction = scanner.nextLine();
			
			switch (instruction) {
			case "1":
				repertoireDesServicesApp();
				break;
			case "2":
				appRun = false;
				break;
			default:
				System.out.println("Mauvais input: sous-menu inexistant.");
				break;
			}
		}
	}

	
}
















