package GYM;

public class InscriptionService {
	private DateTime dateActuelle;
	private Date dateService;
	private String numeroProfessionnel;
	private String nomProfessionnel;
	private String numeroMembre;
	private String nomMembre;
	private String codeService;
	private float prix;

	InscriptionService(DateTime dateActuelle, Date dateService, String numeroProfessionnel, String nomProfessionnel, String numeroMembre, String nomMembre, String codeService, float prix){
		this.dateActuelle = dateActuelle;
		this.dateService = dateService;
		this.numeroProfessionnel = numeroProfessionnel;
		this.numeroMembre = numeroMembre;
		this.codeService = codeService;
		this.nomProfessionnel = nomProfessionnel;
		this.nomMembre = nomMembre;
		this.prix = prix;
	}
	
	
	public DateTime getDateActuelle() {return this.dateActuelle;}
	public Date getDateService() {return this.dateService;}
	public String getNumeroProfessionnel() {return this.numeroProfessionnel;}
	public String getNumeroMembre() {return this.numeroMembre;}
	public String getNomProfessionnel() {return this.nomProfessionnel;}
	public String getNomMembre() {return this.nomMembre;}
	public String getCodeService() {return this.codeService;}
	public float getPrix() {return this.prix;}

}
