package GYM;

import java.io.File;
import java.io.FileWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;


public class CentreDeDonnees {
	private ArrayList<Professionnel> listProfessionnels;
	private ArrayList<Membre> listMembres;
	private ArrayList<ConfirmationSeance> listConfirmationSeances;
	private ArrayList<InscriptionService> listInscriptionServices;
	private ArrayList<Service> listServices;
	private static int numeroCodeMembers;
	private static int numeroCodeProfessionnels;
	
	
	
	CentreDeDonnees(){
		this.listProfessionnels = new ArrayList<Professionnel>();
		this.listMembres = new ArrayList<Membre>();
		this.listConfirmationSeances = new ArrayList<ConfirmationSeance>();
		this.listInscriptionServices = new ArrayList<InscriptionService>();
		this.listServices = new ArrayList<Service>();
		numeroCodeMembers = 100000001;
		numeroCodeProfessionnels = 900000001;
		
	}
	

	//Fini
	public String validationNumeroMembre(String codeAVerifier) {
		for (Membre membre : listMembres) {
			if (membre.getNumeroMembre().equals(codeAVerifier)) {
				return membre.getStatut();
			}
		}
		
		return "Invalide";
	}
	
	//Fini
	public String creationService(DateTime dateCreation, Date dateDebutService, Date dateFinService, Time heureService, String recurrenceHebdomadaire, int capaciteMaximale, String numeroProfessionnel, float prix, String comments, TypeService typeService) {
		try {
			String codeService = "";
			codeService += typeService.getCode();
			Professionnel professionnel = null;
			for (Professionnel prof : this.listProfessionnels) {
				if (prof.getNumeroProfessionnel().equals(numeroProfessionnel)) {
					professionnel = prof;
				}
			}
			
			if (professionnel.getNombreServices()<10) {codeService += "0" + professionnel.getNombreServices();}
			else {codeService += professionnel.getNombreServices();}
			codeService += professionnel.getNumeroProfessionnel().substring(7, 9);
			
			Service service = new Service(dateCreation, dateDebutService, dateFinService, heureService, recurrenceHebdomadaire, capaciteMaximale, numeroProfessionnel, prix, codeService, comments, typeService);
			this.listServices.add(service);
			
			//professionnel.addService(service);
			
			return "Service cree \nCode du service: " + codeService;
		}
		catch (Exception e) {
			return "Erreur dans la creation du service";
		}
		
	}
	
	//Fini
	public String creationMembre(String nom, String email, String infosPersonnelles, Boolean soldePaye) {
		try {
			String codeMembre = "" + numeroCodeMembers;
			numeroCodeMembers++;

			
			Membre membre = new Membre(nom, codeMembre, email, infosPersonnelles, soldePaye);
			this.listMembres.add(membre);
			
			return "Membre cree \nCode du membre: " + codeMembre;
		}
		catch (Exception e) {
			return "Erreur dans la creation du membre";
		}
	}
	
	//Fini
	public String creationProfessionnel(String nom, String email, String infosPersonnelles) {
		try {
			String codeProfessionnel = "" + numeroCodeProfessionnels;
			numeroCodeProfessionnels++;

			
			Professionnel professionnel = new Professionnel(nom, codeProfessionnel, email, infosPersonnelles);
			this.listProfessionnels.add(professionnel);
			
			return "Professionnel cree \nCode du professionnel: " + codeProfessionnel;
		}
		catch (Exception e) {
			return "Erreur dans la creation du professionnel";
		}
	}
	
	//Fini
	public String consultationInscriptions(String codeProfessionnel) {
		String output = "\n";
		
		for (InscriptionService inscription : this.listInscriptionServices) {
			if (inscription.getNumeroProfessionnel().equals(codeProfessionnel)) {
				output += "Code service: " + inscription.getCodeService() + "\n" +
						"Code membre: " + inscription.getNumeroMembre() +  "\n\n";
			}
		}
		
		
		return output;
	}
	
	//Fini
	public String genererRapports() {
		String output = "\n";
		
		for (Professionnel prof : this.listProfessionnels) {
			output += this.genererRapportProfessionnel(prof.getNumeroProfessionnel()) + "\n";
		}
		
		return output;
	}
	
	//Fini
	public String genererRapportProfessionnel(String codeProfessionnel) {
		try {
			String rapport = "";
			String nomProfessionnel = "";
			Professionnel professionnel = null;
			for (Professionnel prof : this.listProfessionnels) {
				if (prof.getNumeroProfessionnel().equals(codeProfessionnel)) {
					nomProfessionnel = prof.getNom();
					professionnel = prof;
				}
			}
			
			if (nomProfessionnel.equals("")) {
				return "Erreur: Professionnel inexistant";
			}
			
			String rapportName = nomProfessionnel + "_";
			DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");  
	        LocalDateTime localDate = LocalDateTime.now();  
	        String dateCreationRapport = dateFormatter.format(localDate);
	        rapportName += dateCreationRapport;
			
			
	        File f = new File("RapportsProfessionnels\\" + rapportName + ".txt");
	        f.createNewFile();
	        
	        rapport += professionnel.getNom() + "\n" +
	        professionnel.getNumeroProfessionnel() + "\n" +
	        professionnel.getInfosPersonnelles();
	        
	        for (ConfirmationSeance service : this.listConfirmationSeances) {
	        	if (service.getNumeroProfessionnel().equals(codeProfessionnel)) {
	        		rapport += "\nService:\n" + service.getDateService().getDate() + "\n" +
	        				service.getDateActuelle().getCompleteDate() + "\n" + 
	        				service.getNomMembre() + "\n" +
	        				service.getNumeroMembre() + "\n" +
	        				service.getCodeService() + "\n" +
	        				service.getPrix() + "$\n";
	        	}
	        }
	        
	        
	        FileWriter fileWriter = new FileWriter("RapportsProfessionnels\\" + rapportName + ".txt");
	        fileWriter.write(rapport);
	        fileWriter.close();
	        
			return rapport;
	
		}
		catch (Exception e) {
			return "Erreur dans la creation du rapport.";
		}
	}
	
	//Fini
	public String genererRapportMembre(String codeMembre) {
		try {
			String rapport = "";
			String nomMembre = "";
			Membre membre = null;
			for (Membre memb : this.listMembres) {
				if (memb.getNumeroMembre().equals(codeMembre)) {
					nomMembre = memb.getNom();
					membre = memb;
				}
			}
			
			if (nomMembre.equals("")) {
				return "Erreur: Membre inexistant";
			}
			
			String rapportName = nomMembre + "_";
			DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");  
	        LocalDateTime localDate = LocalDateTime.now();  
	        String dateCreationRapport = dateFormatter.format(localDate);
	        rapportName += dateCreationRapport;
			
			
	        File f = new File("RapportsMembres\\" + rapportName + ".txt");
	        f.createNewFile();
	        
	        rapport += membre.getNom() + "\n" +
	        membre.getNumeroMembre() + "\n" +
	        membre.getInfosPersonnelles()+ "\n";
	        
	        for (ConfirmationSeance service : this.listConfirmationSeances) {
	        	if (service.getNumeroMembre().equals(codeMembre)) {
	        		rapport += "\nService:\n" + service.getDateService().getDate() + "\n" +
	        				service.getNomProfessionnel() + "\n";
	        		for (TypeService type : TypeService.values()) {
	        			if (type.getCode() + "" == service.getCodeService().substring(0, 3)) {
	        				rapport += type.name() + "\n";
	        			}
	        		}
	        	}
	        }
	        
	        FileWriter fileWriter = new FileWriter("RapportsMembres\\" + rapportName + ".txt");
	        fileWriter.write(rapport);
	        fileWriter.close();
	        
			return rapport;
	
		}
		catch (Exception e) {
			return "Erreur dans la creation du rapport.";
		}
	}
	
	public String procedureComptablePrincipale() {
		for (Professionnel prof : this.listProfessionnels) {
			procedureComptableProfessionnel(prof);
		}
		return "Procedure Comptable finie";
	}
	
	public String procedureComptableProfessionnel(Professionnel professionnel) {
		try {
			String texte = "";
			
			String fileName = professionnel.getNom() + "_";
			DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");  
	        LocalDateTime localDate = LocalDateTime.now();  
	        String dateCreationRapport = dateFormatter.format(localDate);
	        fileName += dateCreationRapport;
			
			
	        File f = new File("TEF\\" + fileName + ".TEF");
	        f.createNewFile();
	        
	        texte += professionnel.getNom() + "\n" +
	        professionnel.getNumeroProfessionnel() + "\n";
	        float solde = 0;
	        for (ConfirmationSeance service : this.listConfirmationSeances) {
	        	if (service.getNumeroProfessionnel().equals(professionnel.getNumeroProfessionnel())) {
	        		solde += service.getPrix();
	        	}
	        }
	        texte += solde + "$\n";
	        
	        FileWriter fileWriter = new FileWriter("TEF\\" + fileName + ".TEF");
	        fileWriter.write(texte);
	        fileWriter.close();
	        
			return "";
	
		}
		catch (Exception e) {
			return "Erreur dans la procedure comptable du professionnel " + professionnel.getNumeroProfessionnel();
		}
	}
	
	//Fini
	public String inscriptionMembre(Date dateService, String numeroMembre, String codeService) {
		
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");  
		LocalDateTime localDateTime = LocalDateTime.now();  
		DateTime dateActuelle = null;
		
		try {
			dateActuelle = new DateTime(dateFormatter.format(localDateTime));
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		Service service = null;
		
		for (Service serv : this.listServices) {
			if (serv.getCodeService().equals(codeService)) {
				service = serv;
			}
		}
		
		String nomProfessionnel = null;
		for (Professionnel prof : this.listProfessionnels) {
			if (prof.getNumeroProfessionnel().equals(service.getNumeroProfessionnel())) {
				nomProfessionnel = prof.getNom();
			}
		}
		
		String nomMembre = null;
		for (Membre memb : this.listMembres) {
			if (memb.getNumeroMembre().equals(numeroMembre)) {
				nomMembre = memb.getNom();
			}
		}
		
		InscriptionService inscriptionService = new InscriptionService(dateActuelle, dateService, service.getNumeroProfessionnel(), nomProfessionnel, numeroMembre, nomMembre, codeService, service.getPrix());
		
		this.listInscriptionServices.add(inscriptionService);
		/*for (Membre memb : this.listMembres) {
			if (memb.getNumeroMembre().equals(numeroMembre)) {
				memb.addInscriptionSeance(inscriptionService);
			}
		}*/
		return "Membre " + numeroMembre + " inscris dans le service " + codeService;
	}
	
	public String confirmationSeance(String numeroMembre, String codeService, String comments) {
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");  
		LocalDateTime localDateTime = LocalDateTime.now();  
		DateTime dateActuelle = null;
		
		try {
			dateActuelle = new DateTime(dateFormatter.format(localDateTime));
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		InscriptionService confirmation = null;
		for (InscriptionService conf : this.listInscriptionServices) {
			if (conf.getCodeService().equals(codeService)){
				confirmation = conf;
			}
		}
		
		ConfirmationSeance confirmationFinale = new ConfirmationSeance(dateActuelle, confirmation.getDateService(), confirmation.getNumeroProfessionnel(), confirmation.getNomProfessionnel(), confirmation.getNumeroMembre(), confirmation.getNomMembre(), confirmation.getCodeService(), comments, confirmation.getPrix());
		
		this.listConfirmationSeances.add(confirmationFinale);
		/*
		for (Membre memb : this.listMembres) {
			if (memb.getNumeroMembre().equals(numeroMembre)) {
				memb.addConfirmationSeance(confirmationFinale);
			}
		}*/
		
		return "Inscription confirmee";
	}
	
	
	//Fini
	public String verificationConfirmationService(String codeService, String codeMembre) {
		try {
			for (InscriptionService inscriptions : this.listInscriptionServices) {
				if (inscriptions.getCodeService().equals(codeService) && inscriptions.getNumeroMembre().equals(codeMembre)) {
					return "Valide";
				}
			}
		
			return "Invalide";
			
		}
		catch (Exception e) {
			return "Mauvais input";
		}
	}
	
	
	/*
	public String trouverServices() {
		return "TODO";
	}
	*/
	
	
}










