package GYM;

public class Professionnel extends Client{

	//private ArrayList<InscriptionService> listInscriptionServices;
	//private ArrayList<ConfirmationSeance> listConfirmationSeances;
	//private ArrayList<Service> listServices;
	private int nombreServices;
	
	Professionnel(String nom, String numero, String email, String infosPersonnelles){
		super(nom, numero, email, infosPersonnelles);
		this.nombreServices	= 0;
	}
	
	public int getNombreServices() { return this.nombreServices; }
	public void augmentNombreServices() { this.nombreServices++; }
	
	/*
	public void addInscriptionSeance(InscriptionService inscriptionService){
		this.listInscriptionServices.add(inscriptionService);
	}
	
	public void addConfirmationSeance(ConfirmationSeance confirmationSeance) {
		this.listConfirmationSeances.add(confirmationSeance);
	}
	*/
	
	/*
	public void addService(Service service) {
		this.listServices.add(service);
	}
	*/
	
	
	public String getNumeroProfessionnel() {
		return super.getNumero();
	}
	
	
	
}
