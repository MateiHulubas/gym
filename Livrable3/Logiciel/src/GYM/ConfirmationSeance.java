package GYM;

public class ConfirmationSeance {
	private DateTime dateActuelle;
	private Date dateService;
	private String numeroProfessionnel;
	private String nomProfessionnel;
	private String nomMembre;
	private String numeroMembre;
	private String codeService;
	private float prix;
	private String commentaires;
	
	ConfirmationSeance(DateTime dateActuelle, Date dateService, String numeroProfessionnel, String nomProfessionnel, String numeroMembre, String nomMembre, String codeService, String comments, float prix){
		this.dateActuelle = dateActuelle;
		this.dateService = dateService;
		this.codeService = codeService;
		this.numeroMembre = numeroMembre;
		this.numeroProfessionnel = numeroProfessionnel;
		this.commentaires = comments;
		this.nomMembre = nomMembre;
		this.nomProfessionnel = nomProfessionnel;
		this.prix = prix;
	}
	
	public DateTime getDateActuelle() {return this.dateActuelle;}
	public Date getDateService() {return this.dateService;}
	public String getNumeroProfessionnel() {return this.numeroProfessionnel;}
	public String getNumeroMembre() {return this.numeroMembre;}
	public String getCodeService() {return this.codeService;}
	public String getCommentaires() {return this.commentaires;}
	public String getNomProfessionnel() {return this.nomProfessionnel;}
	public String getNomMembre() {return this.nomMembre;}
	public float getPrix() {return this.prix;}
	
}
