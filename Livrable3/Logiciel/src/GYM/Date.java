package GYM;

public class Date {
	private int annee;
	private int mois;
	private int jour;
	
	Date(String date) throws Exception{
		this.setDate(date);
	}

	public void setDate(String date) throws Exception{
		if (date.length() == 10) {
			this.jour = Integer.parseInt(date.substring(0, 2));
			this.mois = Integer.parseInt(date.substring(3, 5));
			this.annee = Integer.parseInt(date.substring(6, 10));
		}
		else {
			throw new Exception("Format de Date invalide");
		}
		if (this.jour < 0 && this.mois < 0 && this.annee < 0) {
			throw new Exception("Format de Date invalide");
		}
	}
	
	public String getDate() {
		String string = "";
		if (this.jour < 10) {
			string += "0" + this.jour;
		}
		else {string += this.jour;}
		string += "-";
		
		if (this.mois < 10) {
			string += "0" + this.mois;
		}
		else {string += this.mois;}
		string += "-";
		
		string += this.annee;
		
		return string;
	}
	
	public int getAnnee() {
		return annee;
	}
	public int getMois() {
		return mois;
	}
	public int getJour() {
		return jour;
	}

	
}
