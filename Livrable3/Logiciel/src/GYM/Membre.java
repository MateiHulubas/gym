package GYM;

public class Membre extends Client{
	
	private Statut statut;
	/*
	private ArrayList<InscriptionService> listInscriptionServices;
	private ArrayList<ConfirmationSeance> listConfirmationSeances;
	*/
	
	Membre(String nom, String numero, String email, String infosPersonnelles, Boolean soldePaye){
		super(nom, numero, email, infosPersonnelles);
		if (soldePaye) { this.statut = Statut.Valide; }
		else {this.statut = Statut.Suspendu; }
		
	}
	
	/*
	public void addInscriptionSeance(InscriptionService inscriptionService){
		this.listInscriptionServices.add(inscriptionService);
	}
	
	public void addConfirmationSeance(ConfirmationSeance confirmationSeance) {
		this.listConfirmationSeances.add(confirmationSeance);
	}
	*/
	
	public String getNumeroMembre() {
		return super.getNumero();
	}
	
	public String getStatut() {
    	
    	switch(this.statut) {
    		case Valide:
    			return "Valide";
    		//case Invalide:
    			//return "Invalide";
    		case Suspendu:
    			return "Suspendu";
    	}
    	
		return null;
    }
	
	private enum Statut{
		Valide,
        //Invalide,
        Suspendu
	}
}
 